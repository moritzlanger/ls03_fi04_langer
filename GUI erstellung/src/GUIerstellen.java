import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JColorChooser;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JSplitPane;

public class GUIerstellen extends JFrame {

	private JPanel contentPane;
	private JTextField txtText;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIerstellen frame = new GUIerstellen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIerstellen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 419, 588);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Text");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(12, 13, 367, 45);
		panel.add(lblNewLabel);
		
		JLabel lblHintergrundfarbendern = new JLabel("Hintergrundfarbe \u00E4ndern");
		lblHintergrundfarbendern.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblHintergrundfarbendern.setBounds(12, 71, 367, 17);
		panel.add(lblHintergrundfarbendern);
		
		JLabel lblTextFormatieren = new JLabel("Text formatieren");
		lblTextFormatieren.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTextFormatieren.setBounds(12, 167, 367, 17);
		panel.add(lblTextFormatieren);
		
		JLabel lblSchriftfarbendern = new JLabel("Schriftfarbe \u00E4ndern");
		lblSchriftfarbendern.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSchriftfarbendern.setBounds(12, 293, 367, 17);
		panel.add(lblSchriftfarbendern);
		
		JLabel lblSchriftgreVerndern = new JLabel("Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblSchriftgreVerndern.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSchriftgreVerndern.setBounds(12, 355, 367, 17);
		panel.add(lblSchriftgreVerndern);
		
		JLabel lblTextausrichtung = new JLabel("Textausrichtung");
		lblTextausrichtung.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTextausrichtung.setBounds(12, 414, 367, 17);
		panel.add(lblTextausrichtung);
		
		JLabel lblProgrammBeenden = new JLabel("Programm beenden");
		lblProgrammBeenden.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProgrammBeenden.setBounds(12, 474, 367, 17);
		panel.add(lblProgrammBeenden);
		
		JButton btnNewButton = new JButton("Rot");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.RED);
			}
		});
		btnNewButton.setBounds(12, 97, 115, 25);
		panel.add(btnNewButton);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.green);
			}
		});
		btnGrn.setBounds(138, 97, 115, 25);
		panel.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.blue);
			}
		});
		btnBlau.setBounds(264, 97, 115, 25);
		panel.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(12, 129, 115, 25);
		panel.add(btnGelb);
		
		JButton btnStandartfarbe = new JButton("Standartfarbe");
		btnStandartfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(new Color(0xEEEEEE));
			}
		});
		btnStandartfarbe.setBounds(138, 129, 115, 25);
		panel.add(btnStandartfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color ausgewaehlteFarbe = JColorChooser.showDialog(null, 
			            "Farbauswahl", null);
				panel.setBackground(ausgewaehlteFarbe);
			}
		});
		btnFarbeWhlen.setBounds(264, 129, 115, 25);
		panel.add(btnFarbeWhlen);
		
		txtText = new JTextField();
		txtText.setText("Text");
		txtText.setColumns(10);
		txtText.setBounds(12, 223, 367, 22);
		panel.add(txtText);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Arial", Font.PLAIN, lblNewLabel.getFont().getSize()));
			}
		});
		btnArial.setBounds(12, 190, 115, 25);
		panel.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Comic Sans Ms", Font.PLAIN, lblNewLabel.getFont().getSize()));
			}
		});
		btnComicSansMs.setBounds(138, 190, 115, 25);
		panel.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Courier New", Font.PLAIN, lblNewLabel.getFont().getSize()));
			}
		});
		btnCourierNew.setBounds(264, 190, 115, 25);
		panel.add(btnCourierNew);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText(txtText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(12, 255, 177, 25);
		panel.add(btnInsLabelSchreiben);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.RED);
			}
		});
		btnRot.setBounds(12, 317, 115, 25);
		panel.add(btnRot);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.blue);
			}
		});
		btnBlau_1.setBounds(138, 317, 115, 25);
		panel.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.black);
			}
		});
		btnSchwarz.setBounds(264, 317, 115, 25);
		panel.add(btnSchwarz);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNewLabel.setText("");
				txtText.setText("");
			}
		});
		btnTextImLabel.setBounds(202, 255, 177, 25);
		panel.add(btnTextImLabel);
		
		JButton button_13 = new JButton("-");
		button_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font(lblNewLabel.getFont().getFamily(), Font.PLAIN, lblNewLabel.getFont().getSize() - 1));
			}
		});
		button_13.setBounds(202, 378, 177, 25);
		panel.add(button_13);
		
		JButton button_14 = new JButton("+");
		button_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font(lblNewLabel.getFont().getFamily(), Font.PLAIN, lblNewLabel.getFont().getSize() + 1));
			}
		});
		button_14.setBounds(12, 378, 177, 25);
		panel.add(button_14);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setBounds(12, 436, 115, 25);
		panel.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(138, 436, 115, 25);
		panel.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(264, 436, 115, 25);
		panel.add(btnRechtsbndig);
		
		JButton btnBeenden = new JButton("beenden");
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnBeenden.setBounds(12, 493, 367, 25);
		panel.add(btnBeenden);
	}
}
